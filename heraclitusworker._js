 var    lazy    = require("lazy"),
        fs  = require("fs"),
        neo4j = require('neo4j'),
        db = new neo4j.GraphDatabase(process.env.NEO4J_URL);
var myIndex = 'heraclitus';
var myIndexRelation = 'heraclituswords';
var DEBUG = 2; // 0:none 1:info 2:debug

var indexedWords = {};
var corpusNode;
var wordOccurs = {};
Array.prototype.unique = function() {   
	var o = {}, i, l = this.length, r = [];   
	for(i=0; i<l;i+=1){
		var entry = this[i].trim();
		o[entry] = entry;    
	}
	for(i in o) 
		r.push(o[i]);    
	return r;
};

function splitLineIntoWords( line){
	return line.toLowerCase().replace(/,|\.|;|\-|\(|\)/g,'').slice(1).split(' ').sort().unique();
  
}
function setWordCounts(words){
  for(var i=0;i<words.length;i++){
    if(wordOccurs[words[i]])
      wordOccurs[words[i]]++;
    else
      wordOccurs[words[i]] =1;
  }
}

function debug(statement){
	if(DEBUG >1)
		console.log(statement);
}
function info(statement){
 if(DEBUG >0)
    console.log(statement); 

}
//start with a blank slate

function setup(_){
  db.query("START n = node(*)  MATCH n-[r]-() DELETE n, r",{},_);
  db.query("START n = node(*)  DELETE n",{},_);
  var newNode =  db.createNode({'corpus':'heraclitus'});
  var myNode= newNode.save(_); 
  myNode.index(myIndex,"corpus",'heraclitus');
  info("New corpus" + myNode.id);
  corpusNode = myNode;
  debug(corpusNode);
}
setup(_);
 new lazy(fs.createReadStream('./heraclitus.txt'))
     .lines
     .map(String)
     .map(function(line){
     	var number = line.match(/\d*/);
     	if(number != null) {
      		var words = splitLineIntoWords(line);
          setWordCounts(words);
      		debug(words.length);
      	  	return   {'number':number,'text':line,'words':words};
     	}
     
     })
     .map(function(fragment,_){
     	if(typeof (fragment) != 'undefined' && fragment.number != '' ){
     		info("fragment number " + fragment.number);
     		debug("line " + fragment.text);
     		debug('words ' + fragment.words.join(';'));
			var newFragmentNode =  db.createNode({'number':fragment.number.toString(), 'words':fragment.words, 'text': fragment.text});
			
			var myFragmentNode = newFragmentNode.save(_);
 			myFragmentNode.index(myIndex,"number",fragment.number,_);
 			var relations = myFragmentNode.createRelationshipTo(corpusNode,"fragment",{}, _);
 			debug("." + fragment.words.length);
  			for(var i=0;i<fragment.words.length;i++){
  				var word = fragment.words[i];
  				debug('nr ' + i);
 				if(word =='')
 					continue;
 				debug('word ' +word);
        if (wordOccurs[word] > 20){
          debug('skip frequent word ' + word);
          continue;
        }
 			 	var isWord = db.queryNodeIndex(myIndex,'word:' + word,_);
 				if(isWord.length == 0 ){
 					debug("adding new word " + word)
 					var newNode =  db.createNode({'word':word});
 					var myNode = newNode.save(_);
					myNode.index(myIndex,"word",word);
 					var relations = myNode.createRelationshipTo(myFragmentNode,"word",{}, _);
 					var inds = relations.index(myIndexRelation, 'word',word,_);
  				
 				}
 				else {
					var relations = isWord[0].createRelationshipTo(myFragmentNode,"word",{}, _);
					debug('r');
 				}
 			}
 			info('done fragment');
 }
 

}
)
.tail(function()
  {info('finito') ;});
