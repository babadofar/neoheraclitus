 var    lazy    = require("lazy"),
        fs  = require("fs"),
        neo4j = require('neo4j'),
        urlparse= require('url'),
        http = require('http');
		db = new neo4j.GraphDatabase(process.env.NEO4J_URL);
		var myIndex = 'heraclitus';


http.createServer(function (req, res,_) {
  	req.on("end", function(_){
	  	res.writeHead(200, {'Content-Type': 'text/plain'});
		var qs = urlparse.parse(req.url,true);
		var queryText = qs.query.q;
		
		if(queryText == null){
			res.write('Welcome to the wonderful world of Heraclitus\n');

		}
		else {
			console.log("searching for " + queryText);
			var nodes = db.query('START root=node:heraclitus("word:' + queryText + '") match root-[r]->b  RETURN b.text,b.number',_);
			console.log("got it");
			console.log(nodes)
			for(var nod in nodes){
				res.write(nodes[nod]['b.number']);
				console.log(nodes[nod]['b.number']);
				res.write(nodes[nod]['b.text']);
			}
		
		}
	  res.end('');
	});
}).listen(1337, '127.0.0.1');



console.log('Server running at http://127.0.0.1:1337/');