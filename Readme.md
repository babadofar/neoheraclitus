# Heraclitus neo4j madness/gladnesss\
This is part of the ongoing mission of bringing the message of Heraclitus into the minds of the modern man. 
The goal is to have a working graph of Heraclitus 135 fragments ( or so), and the words/concepts contained in each, displayed on a web page with nice graphics from some cool javascript library. 
To make things more interesting, we will use node.js and neo4j

##  Neo4j
Install neo4j Run neo4j  bin\neo4j.bat
Create an index called heraclits, and a relationship index called heraclituswords

## Node.js
install node.js modules for neo4j
npm install lazy
npm install neo4j
npm install streamline

## Running
first, create the heraclitus database in neo4j.
This is done by the script heraclitus._js
run with streamline.js 

_node heraclitus._js

Once the neo4j is full of heraclitus, try running the displayfication part

_node display._js
Which should create a webserver on port 1337



### Delete all nodes
START n = node(*)  DELETE n;

### Delete all nodes with relationships
 START n = node(*)
      MATCH n-[r]-()
      DELETE n, r;


### Check number of nodes
START n=node(*) return count(n);
START r=rel(*) return count(r);

#### get all nodes
START n=node(*) RETURN n

## using node.js


npm install streamline -g
