var express = require('express');
 var    lazy    = require("lazy"),
        fs  = require("fs"),
        neo4j = require('neo4j'),
		db = new neo4j.GraphDatabase(process.env.NEO4J_URL);
		var myIndex = 'heraclitus';

//start root=node(21607) match root<-[:word]-b--c return c,b;
function printAll(res){
  db.query('START root=node:heraclitus("corpus:heraclitus") match b-[r]->root   RETURN b.number,b.text order by b.number',
    function(err, nodes){

      giveMeNodes(res,nodes);
    }


    );
}
function getAllWords(cb){
  db.query('start root=node:heraclitus("word:*") return root.word;', cb );
}

function giveMeNodes(res,nodes){

  getAllWords(function(err,wordNodes) {

     for(var nod in nodes){
      //     console.log(nodes[nod]['b.number']);
      var mytext = nodes[nod]['b.text'];
  
      var linkedText = mytext.split(' ').map(function(item ){
            if(CheckHas(wordNodes[item])){
                 return '<a href="?q=' + item.toLowerCase().replace(/\.|\,/g,'') + '">' +item + '</a>'; 
               }
            else return item + ' ';

      });
      res.write('<p>');
      res.write('<strong>'+nodes[nod]['b.number'] +'</strong> ');
       linkedText.join(' ');
                   res.write(linkedText);
                  res.write('</p>');
  
    }
        res.end();
});
}
var app = express.createServer( );
app.get('/', function(req, res,_) {
 		var queryText = req.query.q;
		var body = '';
    res.contentType('application/html');
		if(queryText == null){
			
      printAll(res);  
		  
    }

		else {
			console.log ("proc " +process.env.NEO4J_URL) ;
			console.log("searching for " + queryText);
	  db.query('START root=node:heraclitus("word:' + queryText + '") match root-[r]->b  RETURN b.text,b.number',
	   function(err,nodes){
		  	console.log("got it");
			// console.log(nodes)
	     giveMeNodes(res,nodes);
});
	
		}
		
	  
});
 var port = process.env.PORT || 5000;
app.listen(port, function() {
  console.log("Listening on " + port);
});


 